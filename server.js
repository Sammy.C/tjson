const db = require('./api/db')
const ClientController = require('./api/controllers/client');
const TranslatorController = require('./api/controllers/translator');
var express = require('express'),
    app = express(),
    port = process.env.PORT || 3000;

var bodyParser = require('body-parser')


app.use(bodyParser.json())

app.listen(port);
app.post('/api/register', ClientController.create)
app.post('/api/find', ClientController.findOne)
app.post('/api/login', ClientController.login);
app.post('/api/translate', TranslatorController.translate);

console.log('TJson is currently being hosted on this port : ' + port);