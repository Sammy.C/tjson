const db = require('../db')
const bcrypt = require('bcrypt')

const Client = {
    async create(req, res) {
        const query = 'INSERT INTO Client (email,password) VALUES($1, $2);'
        bcrypt.hash(req.body.password, 10).then(function(hash) {
            const values = [req.body.email, hash]
            db.query(query, values, (err, val) => {
                if (err) {
                    console.log("Error occured while inserting Client in database")
                    return res.status(200).send({
                        "status": "Fail",
                        "data": {
                            "registered": false
                        }
                    });
                }
                return res.status(200).send({
                    "status": "Success",
                    "data": {
                        "registered": true
                    }
                });
            })
        });
        
    },
    async findOne(req, res) {
        const query = "Select * from Client where email = $1;"
        const value = [req.body.email]
        db.query(query, value, (err, val) => {
            if (err) {
                console.log("Error occured while retrieve Client with email : " + req.body.email)
                return res.status(400).send(errr)
            }
            if (val.rowCount == 0) {
                return res.status(200).send({
                    "status": "Success",
                    "data": "no user with email : " + req.body.email
                })
            }
            return res.status(200).send({
                "status": "Success",
                "data": {
                    "user": val.rows[0]
                }
            })
        })
    },
    async login(req,res){
        const query = "Select * from Client where email = $1;"
        const value = [req.body.email]
        db.query(query, value, (err, val) => {
            if (err) {
                console.log("Error occured while retrieve Client with email : " + req.body.email)
                return res.status(400).send(errr)
            }
            if (val.rowCount == 0) {
                return res.status(200).send({
                    "status": "fail",
                    "data": "no user with email : " + req.body.email
                })
            }

            const hash = val.rows[0].password;
            bcrypt.compare(req.body.password, hash).then(function(result) {
                if(result){
                    return res.status(200).send({
                        "status": "success",
                        "data": {
                            "login": true
                        }
                    })
                }
                return res.status(200).send({
                    "status": "fail",
                    "data": {
                        "login": false
                    }
                })
            });
        })
    }
}
module.exports = Client