const translatte = require('translatte');

function translateText(text, from, to) {
    return translatte(text, {
        from,
        to,
        services: { "microsoft_v3": { "key": "b8e3fa1f4ce34ea78a2539319093818d" } }
    }).then(res => {
        return res.text;
    }).catch(err => {
        console.error(err);
    });
}

async function translateJsonObj(jsonObj, from, to) {
    let translatedJsonObj = {};

    for (let key in jsonObj) {
        if (typeof jsonObj[key] === 'object') {
            return { 'status': 'fail' }
        }
        translatedJsonObj[key] = await translateText(jsonObj[key], from, to);
    }

    return translatedJsonObj;
}

const Translator = {
    async translate(req, res) {
        let response = {};
        let languages = req.body.to;

        for (let i = 0; i < languages.length; i++) {
            response[languages[i]] = await translateJsonObj(req.body.jsonToTranslate, req.body.from, languages[i]);
        }

        return res.status(200).send(response);
    }
}

module.exports = Translator;