const db = require('../db')


const File = {
    async create(req, res) {
        const query = 'INSERT INTO Files  (name,Client_email) VALUES($1, $2);'
        const values = [req.body.name, req.body.email]
        db.query(query, values, (err, val) => {
            if (err) {
                console.log("Error occured while inserting a File object  in database")
                return res.status(400).send(err)
            }
            return res.status(200).send({
                "status": "Success",
                "data": "File was created in the database"
            });
        })
    },
    async find(req, res) {
        const query = 'Select * from Files where Client_email = $1;'
        const value = [req.body.email]
        db.query(query, value, (err, val) => {
            if (err) {
                console.log("Error occured while inserting a File object  in database")
                return res.status(400).send(err)
            }
            if (val.rowCount == 0) {
                return res.status(200).send({
                    "status": "success",
                    "data": "no files for user with email : " + req.body.email
                })
            }
            return res.status(200).send({
                "status": "success",
                "data": {
                    "files": val.rows
                }
            })
        })
    }
}
module.exports = File